//  DevicePositionControl.swift
//  Copyright © 2019 Aware Inc. All rights reserved.

import UIKit

@IBDesignable class DevicePositionControl: UIStackView {

    //
    // MARK: Properties
    //
    
    var segments = Array<UIButton>()
    var defaultColors = Array<UIColor>()
    
    private var segmentCount: Int = 9
    
    
    //
    // MARK: Initializers
    //
    
    override init(frame: CGRect) {
//        dPrint("[DevicePositionControl | init - frame")
        
        super.init(frame: frame)
        
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
//        dPrint("[DevicePositionControl | init - coder")
        
        super.init(coder: aDecoder)
        
        setup()
    }
    
    
    //
    // MARK: Private Methods
    //
    
    // Initialize, setup segments
    private func setup() {
        
        // Self (stack) properties.
        // NOTE: others set in designer
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.black.cgColor
        
        // Segment's default colors
        defaultColors.append(UIColor.red)
        defaultColors.append(UIColor.orange)
        defaultColors.append(UIColor.yellow)
        defaultColors.append(UIColor.cyan)
        defaultColors.append(UIColor.green)
        defaultColors.append(UIColor.cyan)
        defaultColors.append(UIColor.yellow)
        defaultColors.append(UIColor.orange)
        defaultColors.append(UIColor.red)
        
        for i in 0..<segmentCount {
            // Create segment, use button as segment
            let button = UIButton()
            
            // Set segments' properties
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.black.cgColor
            button.layer.cornerRadius = 4
            
            // Initialize all segments to clear
            button.backgroundColor = UIColor.clear
            
            // Set segements' constraints here
            // Disable automatically generated constraints, replace
            // the autogenerated constraints with own.
            button.translatesAutoresizingMaskIntoConstraints = false

            segments.append(button)
            
            self.addArrangedSubview(segments[i])
        }
        
        // Set first one to be active
        segments[0].backgroundColor = defaultColors[0]
        
        // Evenly distribute stackView segments
        self.distribution = .fillEqually
    }
    
    private func clearAllSegments() -> Void {
        
        for seg in segments {
            seg.backgroundColor = UIColor.clear
        }
    }
    
    //
    // MARK: Public or internal methods
    //
    
    private func getActiveSegment() -> Int {
        
        for i in 0..<segmentCount {
            if !(segments[i].backgroundColor?.isEqual(UIColor.clear))! {
                return i
            }
        }
        
        // TODO: Should never hit this.
        return -1
    }
    
    private func getNewActiveSegment(y: CGFloat) -> Int {
        
        if y < -0.6  && y >= -1.0 {
            return 0
        }
        else if y < -0.40 && y >= -0.60 {
            return 1
        }
        else if y < -0.15 && y >= -0.40 {
            return 2
        }
        else if y < -0.05 && y >= -0.15 {
            return 3
        }
        // Center:
        // NOTE: Center means device is in position
        else if y > -0.05 && y <= 0.05 {
            return 4
        }
        else if y >= 0.05 && y < 0.15 {
            return 5
        }
        else if y >= 0.15 && y < 0.40 {
            return 6
        }
        else if y >= 0.40 && y < 0.60 {
            return 7
        }
        else if y >= 0.60 && y <= 1.0 {
            return 8
        }
        
        return -1
    }
    
    // Set colors of device position segments
    func updateSegments(
        y: CGFloat, isInPosition: Bool) {
        
        // clearAllSegments()
        let currentActiveSegment = getActiveSegment()
        let newActiveSegment = getNewActiveSegment(y: y)
        
        // Clear current, colorize new if different
        if currentActiveSegment != newActiveSegment {
            
            clearAllSegments()
            
            // Use default colors when not in position. Otherwise
            // when in position, make segment green
            if isInPosition {
                let centerSegment = 4
                let direction = (centerSegment - newActiveSegment) > 0 ? -1 : 1
                
                for index in stride(from: centerSegment, through: newActiveSegment, by: direction) {
                    segments[index].backgroundColor = UIColor.green
                }
            } else {
                segments[newActiveSegment].backgroundColor = defaultColors[newActiveSegment]
            }
        }
    }
}
